import Api from './api.js'

class Swapi extends Api {
  constructor () {
    const url = ''
    super(url)
  }

  /**
   * Fetch all starships iterating over the pagination
   * @param passengers
   */
  async fetchAllStarships (passengers) {

    let wasPartOf = []

    const fetchAllPages = async (page = 1, results = []) => {
      this.url = 'https://swapi.co/api/starships/'

      this.params = {
        page: page
      }

      let response = await this.fetch()
      results.push(...response.results)

      if (response.next) {
        await fetchAllPages(page + 1, results)
      }

      return results
    }

    let ships = await fetchAllPages()

    let hasEpisodes = await ships.filter(ship => ship.films.length >= 3)

    let hasCapacity = await hasEpisodes.filter((ship) => {
      return this.canCarryPassengers(ship, passengers)
        && this.canTravelForAWeek(ship)
    })

    for (let i = 0; i < hasCapacity.length; i++) {
      if (await this.wasPartOfTheTrilogy(hasCapacity[i])) {
        wasPartOf[wasPartOf.length] = hasCapacity[i]
      }
    }

    if (wasPartOf.length > 1) {
      wasPartOf.sort((a, b) => (a.hyperdrive_rating > b.hyperdrive_rating) ? -1 : ((b.hyperdrive_rating > a.hyperdrive_rating) ? 1 : 0))
    }

    return wasPartOf[0]
  }

  /**
   * Can carry the given number of passengers?
   * @param ship
   * @param passengers
   * @returns {boolean}
   */
  canCarryPassengers (ship, passengers) {
    return parseInt(ship.passengers) >= passengers
  }

  /**
   * Can travel for at least a week?
   * @param ship
   * @returns {boolean}
   */
  canTravelForAWeek (ship) {
    const time = ship.consumables.split(' ')

    // Skip the unknown
    if (time.length === 2) {
      return (!('days' === time[1] && parseInt(time[0]) < 7))
    } else {
      return false
    }
  }

  /**
   * Was this ship part of the original trilogy?
   * @param ship
   * @returns {boolean}
   */
  async wasPartOfTheTrilogy (ship) {
    let films = ship.films
    let episodes = []
    let was = false

    for (let i = 0; i < films.length; i++) {
      let response = await fetch(films[i])
      let data = await response.json()

      episodes.push(data.episode_id)

      if (episodes.length === films.length) {
        was = episodes.includes(4) && episodes.includes(5) && episodes.includes(6)
      }
    }

    return was
  }
}

export default Swapi