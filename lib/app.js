import RandomUserApi from './random-user.js'
import Swapi from './swapi.js'

/**
 * Renders a user markup
 * @param container
 * @param user
 */
const userMarkup = (container, user) => {
  let loader = document.getElementById('loading')
  loader && loader.remove()

  let userContainer = document.createElement('div')
  userContainer.className = 'user'

  let nameContainer = document.createElement('p')
  nameContainer.innerHTML = `${user.name.first} ${user.name.last}, ${user.name.title}`
  userContainer.appendChild(nameContainer)

  let genderContainer = document.createElement('p')
  genderContainer.innerHTML = `<strong>Gender:</strong> ${user.gender}`

  let ageContainer = document.createElement('p')
  ageContainer.innerHTML = `<strong>Age:</strong> ${user.dob.age}`

  let separator = document.createElement('hr')

  container.appendChild(userContainer)
  container.appendChild(genderContainer)
  container.appendChild(ageContainer)
  container.appendChild(separator)
}

/**
 * Render 10 users ordered by name
 */
const fetchAndOrder = () => {
  const api = new RandomUserApi()
  let container = document.querySelector('#main')

  api.fetchAndOrder()

  api.users.then(users => {
    users.map((user) => userMarkup(container, user))

    console.log('%c Output as required by the test (array of users):', 'font-size: 16px; color: #ff0000')
    console.log(users)
  })
}

/**
 * Render a user older than the age parameter
 */
const fetchAndFind = (age) => {
  const api = new RandomUserApi()
  let container = document.querySelector('#main')
  let ageSpan = document.querySelector('#age')

  ageSpan.innerText = age

  api.fetchAndFind(age)

  api.users.then(user => {
    userMarkup(container, user)

    console.log('%c Output as required by the test (User data):', 'font-size: 16px; color: #ff0000')
    console.log(user)
  })
}

/**
 * Fetch users and count the most used character
 */
const fetchAndCount = () => {
  const api = new RandomUserApi()
  let container = document.querySelector('#main')
  let countContainer = document.querySelector('#count')

  api.fetchAndCount().then(element => {
    countContainer.innerText = `The most used character is "${element.char}" and it's used ${element.value} times`

    console.log('%c Output as required by the test (String of most used char):', 'font-size: 16px; color: #ff0000')
    console.log(element.char)
  })

  api.users.then(users => users.map((user) => userMarkup(container, user)))
}

/**
 * Returns the fastest ship
 * @param passengers
 */
const fastestShip = async (passengers) => {
  const api = new Swapi()
  let container = document.querySelector('#main')
  let countContainer = document.querySelector('#count')

  let ship = await api.fetchAllStarships(passengers)

  if(ship) {
    let loader = document.getElementById('loading')
    loader && loader.remove()
    countContainer.innerText = `Passengers to carry: ${passengers}`

    let nameContainer = document.createElement('p')
    nameContainer.innerHTML = `<h4>${ship.name} (${ship.model})</h4>`

    let passengersContainer = document.createElement('p')
    passengersContainer.innerHTML = `<strong>Passengers:</strong> ${ship.passengers}`

    let consumablesContainer = document.createElement('p')
    consumablesContainer.innerHTML = `<strong>Can travel for:</strong> ${ship.consumables}`

    let speedContainer = document.createElement('p')
    speedContainer.innerHTML = `<strong>Hyperdrive:</strong> ${ship.hyperdrive_rating}`

    let separator = document.createElement('hr')

    container.appendChild(nameContainer)
    container.appendChild(passengersContainer)
    container.appendChild(consumablesContainer)
    container.appendChild(speedContainer)
    container.appendChild(separator)

    console.log('%c Output as required by the test (Ship data):', 'font-size: 16px; color: #ff0000')
    console.log(ship)
  }
}

export { fetchAndOrder, fetchAndFind, fetchAndCount, fastestShip }