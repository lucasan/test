import Api from './api.js'
import Utils from './utils.js'

class RandomUserApi extends Api {

  constructor () {
    const url = 'https://randomuser.me/api'
    super(url)

    this._users = []
  }

  set users (users) {
    this._users = users
  }

  get users () {
    return this._users
  }

  /**
   * Fetch a given number of users and return them order by a property
   * @param property
   * @param amount
   * @returns {RandomUserApi}
   */
  getSortedByProperty (property, amount = 1) {
    this.params = {
      results: amount
    }

    this.users = this.fetch().then((response) => {
      if (response.results) {
        const users = response.results

        switch (property) {
          case 'name':
            users.sort(Utils.sortByFirstName)
            break
          case 'age':
            users.sort(Utils.sortByAge)
            break
        }

        return users
      }
    })
  }

  /**
   * 1st Exercise: Fetch & Order
   */
  fetchAndOrder () {
    this.getSortedByProperty('name', 10)
  }

  /**
   * 2nd Exercise: Fetch & Find
   * Load 100 records, iterate over them and if no user is found call itself recursively
   * Retrieving multiple records is cheaper than making multiple http calls
   * @param age
   */
  fetchAndFind (age) {
    this.getSortedByProperty('age', 100)
    let found = false

    this.users = this.users.then((users) => {
      for (let i = 0; i < users.length; i++) {
        if (age < users[i].dob.age) {
          found = true
          return users[i]
        }
      } 
      !found && this.fetchAndFind(age)
    })
  }

  /**
   * 3rd Exercise: Fetch & count
   * Load 5 users and return the most used character in their names
   * @returns {PromiseLike<{char: string, value: number}> | Promise<{char: string, value: number}>}
   */
  fetchAndCount () {
    this.getSortedByProperty('age', 5)
    let hash = {}

    return this.users.then((users) => {

      users.map((user) => {
        hash = `${user.name.first}${user.name.last}`.toLowerCase().split('').reduce((total, char) => {
          total[char] ? total[char]++ : total[char] = 1
          return total
        }, hash)
      })

      let prev = 0
      let result = {char: '', value: 0}
      for (let [c, v] of Object.entries(hash)) {
        result = v > prev ? {char: c, value: v} : result
        prev = v > prev ? v : prev
      }

      return result
    })
  }
}

export default RandomUserApi