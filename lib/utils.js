/**
 * Util class
 */
class Utils {

  /**
   * Helper to sort an array of users by first name
   * @param a
   * @param b
   * @returns {number}
   */
  static sortByFirstName = (a, b) => (a.name.first > b.name.first) ? 1 : ((b.name.first > a.name.first) ? -1 : 0)

  /**
   * Helper to sort an array of users by age
   * @param a
   * @param b
   * @returns {number}
   */
  static sortByAge = (a, b) => (a.dob.age > b.dob.age) ? 1 : ((b.dob.age > a.dob.age) ? -1 : 0)
}

export default Utils