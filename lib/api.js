/**
 * API class
 */
class Api {
  constructor (url) {
    this._url = url
    this._params = {}
  }

  get url () {
    return this._url
  }

  set url (url) {
    this._url = url
  }

  get params () {
    return this._params
  }

  set params (params) {
    this._params = new URLSearchParams(params).toString()
  }

  /**
   * Fetch a resource from a given API url
   * @returns {Promise<any>}
   */
  fetch () {
    const url = `${this.url}?${this.params}`

    return fetch(url)
      .then(function (response) {
        if (!response.ok) {
          throw new Error(`HTTP error, status = ${response.status}`)
        }
        return response.json()
      })
      .catch(function (error) {
        throw new Error(`API Internal error = ${error.message}`)
      })
  }
}

export default Api