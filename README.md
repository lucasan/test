# Lucas Torres' Rootstack test

Hi, thanks for taking the time to review my work, please let me add some considerations about it:

## Considerations

- As a personal challenge, I decided to pick the language I have the less experience with, JavaScript.

- I decided to use the most recent Js syntax (ES6), and it's cool, but this demo only works in Chrome. For a production like scenario, a Compiler like *Babel* would do the compatibility job. But to comply with the test requirements, I kept it Vanilla.

- I decided to add markup to show the results, but if you want to see the output in the format required by the test, just open up your Chrome Console and you''l see them.

- Doing the tests on weekdays without affecting my current job was hard because of the time, so I only delivered the first 4 exercises. But will follow up during the weekend to deliver the last one.

## How and where to see the test 

You can visit this url to see the test in action https://luke-rootstack.netlify.com/

Or you can clone the repository and run it in your local machine.

### Checking in your local machine
Since the most recent JS syntax uses modules, clicking the `index.html` file won't be enough.

If you're behind a Mac, you can follow this steps:

1. Open the MacOS terminal
2. Browse to the folder of the repository
3. Type in `python -m SimpleHTTPServer`
4. Go to your Chrome Browser and navigate to `http://127.0.0.1:8000`

## Thanks
Don't hesitate to contact me if you have any doubt or want to talk about this.

Lucas Torres <lucasan37@gmail.com>